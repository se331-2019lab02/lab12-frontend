import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { StudentService } from './student-service';
import { Observable, throwError } from '../../../node_modules/rxjs';
import Student from '../entity/student';
import { map, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ERROR_COMPONENT_TYPE } from '@angular/compiler';
@Injectable({
  providedIn: 'root'
})

export class StudentsFileImplService extends StudentService {
  saveStudent(student: Student): Observable<Student> {
    throw new Error("Method not implemented.");
  }
  getStudent(id: number): Observable<Student> {
    return this.http.get<Student[]>('http://localhost:8080/students')
      .pipe(map(students => {
        const output: Student = (students as Student[]).find(student => student.id === +id);
        return output;
      }));

  }

  constructor(private http: HttpClient) {
    super();
  }
  uploadFile(image:File): Observable<any>{
    const formData: any= new FormData();
    formData.append('file',image);
    return this.http.post(environment.uploadApi,formData,{
      reportProgress:true,
      observe:'events',
      responseType:'text'
    }).pipe(
      catchError(this.errorMgmt)
    )
  }

  errorMgmt(error: HttpErrorResponse){
    let errorMessage = '';
    if(error.error instanceof ErrorEvent){
      // Get client-side error
      errorMessage = error.error.message;

    }
    else{
      //Get server-side error
      errorMessage = 'Error Code ${error.status}\nMessage:${error.message}';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
  getStudents(): Observable<Student[]> {
    return this.http.get<Student[]>('http://localhost:8080/students');
  }
}
