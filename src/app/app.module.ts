import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { StudentService } from './service/student-service';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { StudentsFileImplService } from './service/students-file-impl.service';
import { StudentsComponent } from './students/list/students.component';
import { StudentsAddComponent } from './students/add/students.add.component';
import { StudentsViewComponent } from './students/view/students.view.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyNavComponent } from './my-nav/my-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule, MatButtonModule, MatSidenavModule
  , MatIconModule, MatListModule, MatGridListModule, MatCardModule
  , MatMenuModule, MatTableModule, MatPaginatorModule, MatSortModule, MatProgressSpinnerModule, MatRadioModule, MatProgressBarModule
} from '@angular/material';
import {MatInputModule} from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';
import { StudentRoutingModule } from './students/student-routing.module';
import { StudentTableComponent } from './students/student-table/student-table.component';
import { StudentRestImplService } from  './service/student-rest-impl.service';
import {MatFileUploadModule} from "mat-file-upload"
import { from } from 'rxjs';
import { FileUploadService } from './service/file-upload.service';
import { CourseComponent } from './course/course.component';
import { ListComponent } from './course/list/list.component';
import { AddComponent } from './course/add/add.component';
import { InfoComponent } from './course/info/info.component';
import { MatSelectModule } from '@angular/material/select';
import { CourseService } from './service/course-service';
import { CourseRestImplService } from './service/course-rest-impl.service';
@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent,
    StudentsAddComponent,
    StudentsViewComponent,
    MyNavComponent,
    FileNotFoundComponent,
    StudentTableComponent,
    CourseComponent,
    ListComponent,
    AddComponent,
    InfoComponent,
  
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatRadioModule,
    MatFileUploadModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatProgressBarModule,
    StudentRoutingModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    MatSelectModule
  ],
  providers: [
    { provide: StudentService, useClass: StudentsFileImplService  },
    {provide: CourseService, useClass: CourseRestImplService}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
