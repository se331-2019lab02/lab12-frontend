import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { InfoDataSource, InfoItem } from './info-datasource';
import Student from 'src/app/entity/student';
import { Params, ActivatedRoute } from '@angular/router';
import { StudentService } from 'src/app/service/student-service';
import Course from 'src/app/entity/course';
import { CourseService } from 'src/app/service/course-service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit{
  ngOnInit(): void {
    this.route.params
     .subscribe((params: Params) => {
     this.courseService.getCourse(+params['id'])
     .subscribe((inputCourse:Course) => this.course = inputCourse);
     });
    
  }
  course:Course
  constructor(private route: ActivatedRoute, private courseService: CourseService){}
  courses: Course[];
}

