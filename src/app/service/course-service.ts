import { Observable } from '../../../node_modules/rxjs';
import Course  from '../entity/course';


export abstract class CourseService {
     abstract getCourses(): Observable<Course[]>;
     abstract getCourse(id: number): Observable<Course>;
     abstract saveCourse(course: Course): Observable<Course>;
}
