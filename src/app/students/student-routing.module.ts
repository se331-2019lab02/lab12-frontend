import { Routes, RouterModule } from '@angular/router';
import { StudentsViewComponent } from './view/students.view.component';
import { StudentsAddComponent } from './add/students.add.component';
import { StudentsComponent } from './list/students.component';
import { NgModule } from '@angular/core';
import { StudentTableComponent } from './student-table/student-table.component';
import { CourseComponent } from '../course/course.component';
import { ListComponent } from '../course/list/list.component';
import { AddComponent } from '../course/add/add.component';
import { InfoComponent } from '../course/info/info.component';




const StudentRoutes: Routes = [
    { path: 'add', component: StudentsAddComponent },
    { path: 'list', component: StudentTableComponent },
    { path: 'detail/:id', component: StudentsViewComponent },
    { path: 'table', component: StudentTableComponent },
    { path: 'courseList', component: ListComponent},                                                                                                                                                                                                                                                                                                                                                             { path: 'courseList', component: ListComponent},
    { path: 'courseAdd', component: AddComponent},
    //{ path: 'courseInfo', component: InfoComponent},
    { path: 'coursedetail/:id', component:  InfoComponent },
];
@NgModule({
    imports: [
        RouterModule.forRoot(StudentRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class StudentRoutingModule {

}
