export default class Course {
    id:string;
    courseId:string;
    courseName:string;
    content:string; 
    lecturer:string;
}
