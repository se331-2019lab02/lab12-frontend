import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import Course  from 'src/app/entity/course';
import { CourseService } from 'src/app/service/course-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent {
  courses:Course[]
  uploadEndPoint: string;
  form = this.fb.group({
    courseId:[null, Validators.compose([Validators.required, Validators.maxLength(13)])],
    courseName:[null, Validators.required],
    content:[null, Validators.required],
    lecturer:[null, Validators.required]
  });
  validation_messages = {
    'courseId': [
      { type: 'required', message: 'course id is required' },
      { type: 'maxlength', message: 'course id is too long' }
    ],
    'courseName': [
      { type: 'required', message: 'the lecturer is required' }
    ],
    'content': [
      { type: 'required', message: 'the lecturer is required' }
    ],
    'lecturer': [
      { type: 'required', message: 'the lecturer is required' }
    ],
  };
  


submit() {

   alert('save')
  }
  constructor(private fb: FormBuilder) {}
}
